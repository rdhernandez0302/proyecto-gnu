### ArchitectUI - Angular 7 Bootstrap 4

Este proyecto fue generado con  [Angular CLI](https://github.com/angular/angular-cli) versión 7.3.0.

## Servidor de desarrollo

Corre `ng serve` para un servidor de desarrollo. Navegar a `http://localhost:4200/`. La aplicación se recargará automáticamente si cambia alguno de los archivos de origen.

## Otra configuración
Si desea cambiar el puerto en el que se ejecutara el backend(por defecto es el 3000), debera modificar el siguiente archivo: frontend\src\congig\config.config.ts, actualizando el puerto por el deseado.
