import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Pages
import { UsersComponent } from './BaseViews/Users/users.component';

const routes: Routes = [
  {path: 'usuarios', component: UsersComponent},
  
  {path: '**', redirectTo: 'usuarios'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
