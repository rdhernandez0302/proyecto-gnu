import {Injectable} from "@angular/core";
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class MessageResponseHelper {
    constructor() {

    }
   
    success(_response: string){
      const message = this.message(_response);
      Swal.fire({
          type: 'success',
          position: 'top-end',
          title: message,
          showConfirmButton: false,
          timer: 1500
      });   
    }
    
    danger(_response: any){
      if( ! _response){
        return;
      }

      const message = this.message(_response);
      
      Swal.fire({
        title: 'Alerta!',
        text: message,
        type: 'error',
        confirmButtonText: 'Cerrar'
      });   
    }

    message(_response: any){
      let message: string = '';
      if(_response.message.length > 0){
        if( ! _response.message[0].constraints) {
          message = _response.message; 
        }else{
          for (const [key, value] of Object.entries(_response.message[0].constraints)) {
            message = String(value); 
          }
        }
      }
      return message;
    }
}
