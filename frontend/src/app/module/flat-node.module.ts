/** Flat to-do item node with expandable and level information */
export interface FlatNode {
  expandable: boolean;
  name: string;
  id: number;
  level: number;
}