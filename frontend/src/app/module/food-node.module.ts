export interface FoodNode {
    id?:number;
    name?: string;
    children?: FoodNode[];
}