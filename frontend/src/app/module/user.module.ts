export interface UserData {
    iduser: string;
    name: string;
    email: string;
    username: string;
    name_role: string;
    action: string;
}
  