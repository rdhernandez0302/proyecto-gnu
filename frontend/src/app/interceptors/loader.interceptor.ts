import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import {catchError} from 'rxjs/internal/operators';
import {throwError} from 'rxjs';
import { LoaderService } from '../services/loader.service';
import { MessageResponseHelper } from '../helper/message-response.helper';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { BASE_URL_API } from '../config/config.config';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
    constructor(public _loaderService: LoaderService,
                private _messageResponseHelper: MessageResponseHelper,
                private _route: Router,
                private _modalService: NgbModal,
                private _userService: UserService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this._loaderService.show();
        return next.handle(req).pipe(
            catchError(error => {
                this._messageResponseHelper.danger(error.error);
                this._loaderService.hide();
                return throwError(error);
            }),
            finalize(() => {
                this._loaderService.hide();
            })
        );
    }
}

