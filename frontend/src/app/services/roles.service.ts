import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL_API } from '../config/config.config';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private _http: HttpClient) { }

  getAll(){
    return this._http.get(`${BASE_URL_API}/roles/list`);
  }

  listRoles(_data){
    return this._http.post(`${BASE_URL_API}/roles/list-roles`, _data);
  }

  
}
