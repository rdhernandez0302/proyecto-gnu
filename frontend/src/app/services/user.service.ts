import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BASE_URL_API } from '../config/config.config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) {}

  login(data: any) {
    return this._http.post(`${BASE_URL_API}/auth/signin`, data);
  }

  validateToken(_token: string) {
    const headers = new HttpHeaders({token: _token });
    const options = { headers };
    this._http.post(`${BASE_URL_API}/auth/token-validate`, {}, options).subscribe((response:any) =>{
      if(response.message[0].value.token){
        localStorage.setItem('ACCESS_TOKEN', response.message[0].value.token);
      }
    });
  }

  listUsers(_data){
    return this._http.post(`${BASE_URL_API}/users/list-users`, _data);
  }
  
  getById(_iduser:number){
    return this._http.get(`${BASE_URL_API}/users/list-by-id/${_iduser}`);
  }

  changeStatus(_iduser:number){
    return this._http.post(`${BASE_URL_API}/users/change-status`,{iduser: _iduser});
  }

  changePassword(_data){
    return this._http.post(`${BASE_URL_API}/users/change-password`, _data);
  }

  changePasswordGeneral(_token: string, _data) {
    const headers = new HttpHeaders({token: _token });
    const options = { headers };
    return this._http.post(`${BASE_URL_API}/users/change-password-general`, _data, options);
  }

  create(_data:any){
    return this._http.post(`${BASE_URL_API}/users/create`, _data);
  }

  update(_data:any){
    return this._http.post(`${BASE_URL_API}/users/update`, _data);
  }

}
