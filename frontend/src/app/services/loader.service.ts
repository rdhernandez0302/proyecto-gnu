import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Injectable()
export class LoaderService {
    constructor(private loadingBar: LoadingBarService){}

    isLoading = new Subject<boolean>();
    show() {
        this.isLoading.next(true);
        this.loadingBar.start();
    }
    hide() {
        this.isLoading.next(false);
        this.loadingBar.complete();
    }
}