import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AppComponent } from '../../app.component';
import { MatTableDataSource } from '@angular/material';
import { UserData } from '../../module/user.module';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { MessageResponseHelper } from '../../helper/message-response.helper';
import { RolesService } from '../../services/roles.service';
import { MustMatch } from 'src/app/helper/mush-match.helper';
import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})

export class UsersComponent {
  elementData = [];
  displayedColumns = ['name', 'username', 'email', 'name_role', 'action'];
  dataSource: MatTableDataSource<UserData>;
  registerForm: FormGroup;
  changePasswordForm: FormGroup;
  saveOrUpdate = false; 
  submitted = false;
  pageSize = 10;
  currentPage = 0;
  totalSize = 0;
  array: any;
  resultsLength = 0;
  search = '';
  order = '';
  roles = '';
  activeOrInactive = true;
  username = '';
  iduser = '';
  validatorPassword = [Validators.required, Validators.minLength(6)];
  
  UserDatag = 'Usuarios';
  subheading = 'Lista de usuarios registrados';
  icon = 'pe-7s-users icon-gradient bg-tempting-azure';

  constructor(private _appComponent: AppComponent,
              private _modalService: NgbModal, 
              private _formBuilder: FormBuilder,
              private _userService: UserService,
              private _messageResponseHelper: MessageResponseHelper,
              private _rolesService: RolesService,) {
                _appComponent.setTitle("Usuarios");
                this.loadMain();
                this.loadPermissionsButton();
  }

  loadPermissionsButton(){
    const token = localStorage.getItem('ACCESS_TOKEN');
    const pathRoute = 'users';
  
  }

  loadMain(){
    this.registerForm = this._formBuilder.group({
      iduser: [''],
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: [''],
      idrole: ['', Validators.required],
    });

    this.changePasswordForm = this._formBuilder.group({
      iduser: [''],
      password: ['', this.validatorPassword],
      repitPassword: ['', this.validatorPassword],
    }, {
      validator: MustMatch('password', 'repitPassword')
    });

    this.loadData();
    this.loadRoles();
  }

  public handlePage(_e: any) {
    this.currentPage = _e.pageIndex * _e.pageSize;
    this.pageSize = _e.pageSize;
    this.loadData();
  }

  sortData(_e) {
    this.order = _e;
    this.loadData();
  }

  loadData(){
    const value={
      draw: 1,
      order: [this.order],
      start: this.currentPage,
      length: this.pageSize,
      search: {value: this.search, regex: false}
    };
    this._userService.listUsers(value).subscribe( (response: any) => {
      console.log(response.data.length);
      for (let i = 0; i < response.data.length; i++) {        
        var json = {nombre: response.data[i][1], usuario:  response.data[i][2], correo: response.data[i][3], rol: response.data[i][4]};
        this.elementData.push(json);  
        console.log(this.elementData);
      }
      
      this.dataSource = new MatTableDataSource(response.data);
      this.resultsLength = response.recordsFiltered;
    });
  }

  loadRoles(){
    this._rolesService.getAll().subscribe( (response: any) => {
      this.roles = response;
    });
  }

  applyFilter(_filterValue: string) {
    _filterValue = _filterValue.trim(); // Remove whitespace
    _filterValue = _filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.search = _filterValue;
    this.loadData();
  }

  openModal(_content){
    this._modalService.open(_content, {
      size: 'lg'
    });
  }

  openModalSaveOrUpdate(_content, _iduser: boolean | number) {
    if(_iduser) {
      this.saveOrUpdate = true;
      this._userService.getById(Number(_iduser)).subscribe( (response: any) => {
        this.registerForm.get('password').clearValidators();
        this.registerForm.get('password').updateValueAndValidity();
        this.registerForm.patchValue(response);
        this.openModal(_content);
      });
    }else {
      this.registerForm.get('password').setValidators(this.validatorPassword);
      this.saveOrUpdate = false;
      this.registerForm.reset();
      this.openModal(_content);
    }

    this.submitted = false;
  } 

  openModalActiveOrInactive(_content, _iduser: boolean | number) {
    if(_iduser) {
      this._userService.getById(Number(_iduser)).subscribe( (response: any) => {

        if(response.status == 2){
          this.activeOrInactive = true;
        }else{
          this.activeOrInactive = false;
        }

        this.username = '"'+response.username+'"';
        this.iduser = response.iduser;
        this.openModal(_content);
      });
    }
  }

  openModalChangePassword(_content, _iduser: boolean | number) {
    if(_iduser) {
      this.changePasswordForm.reset();
      this.submitted = false;
      const that = this;
      this._userService.getById(Number(_iduser)).subscribe( (response: any) => {
        this.username = '"'+response.username+'"';
        this.iduser = response.iduser;
        this.changePasswordForm.patchValue(response);
        that.openModal(_content);
      });
    }
  }

  changeStatus(){
    if(this.iduser) {
      this._userService.changeStatus(Number(this.iduser)).subscribe( (response: any) => {
        this._messageResponseHelper.success(response);
        this.loadData();
        this._modalService.dismissAll();
      });
    }
  }

  changePassword(){

    this.submitted = true;
    // stop here if form is invalid
    if (this.changePasswordForm.invalid) {
        return;
    }

    delete this.changePasswordForm.value.repitPassword;
    this._userService.changePassword(this.changePasswordForm.value).subscribe( (response: any) => {
      this._messageResponseHelper.success(response);
      this._modalService.dismissAll();
    });
  }

  createOrUpdate() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    if( this.saveOrUpdate ) {
      this._userService.update(this.registerForm.value).subscribe( (response: any) => {  
        this._messageResponseHelper.success(response);
        this.loadData();
        this._modalService.dismissAll();
      });
    }else{
      this._userService.create(this.registerForm.value).subscribe( (response: any) => {
        this._messageResponseHelper.success(response);
        this.loadData();
        this._modalService.dismissAll();
      });
    }
  }
  get f() { return this.registerForm.controls; }

  displayCssFor(_field: string|Array<string>){
    return (this.submitted && this.registerForm.get(_field).invalid ) ? 'is-invalid' : '';
  }

  get g() { return this.changePasswordForm.controls; }

  displayCssForPassword(_field: string|Array<string>){
    return (this.submitted && this.changePasswordForm.get(_field).invalid ) ? 'is-invalid' : '';
  }

  exportTable() {

    var wb = XLSX.utils.book_new();
    var ws = XLSX.utils.json_to_sheet(this.elementData);
    XLSX.utils.book_append_sheet(wb, ws, "HOJA 1");
    XLSX.writeFile(wb, `nombrearchivo.xlsx`);

  }

}
