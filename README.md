# Proyecto base GNU

### Inicio
Primero debemos clonar el repositorio en la carpeta de nuestro servidor web
- ###### Por ejemplo
  `$ git clone https://gitlab.com/rdhernandez0302/project-base.git`
  `$ cd project-base` 

### Exportar base de datos
Se importa la base de datos, se recomienda el uso de mysql.

### Programas utilizados
- ##### Backend
    <pre>
    Os versión: windows 10
    NodeJs versión: 10.16.3
    Npm versión: 6.9.0
    <hr>
    NestJs versión: 6.12.3 (framework)  
    </pre>

- ##### Frontend
    <pre>
    Npm versión: 6.9.0
    Angular 7 Bootstrap 4    
    <hr>
    Angular CLI version 7.3.0.    
    </pre>

### Siguiente paso
Una vez realizado las instalaciones anteriores, deberá dirigirse a la carpeta backend y frontend, en donde encontrará la respectiva configuración para el manejo.