# Project base gnu

### Nota
Para compilar los siguientes comandos es necesrio ingresar a la carpeta <b>backend</b> y no quedarse en la raíz

### Instalación del paquete

> npm install 

> npm i -g @nestjs/cli

### Configuración predeterminada
- ##### Crear el archivo .env en la ruta padre c:\nombre-proyecto\backend\.env

    <pre>
        PORT=3000
        MYSQL_HOST=localhost
        MYSQL_USERNAME=root
        MYSQL_PASSWORD=
        MYSQL_DATABASE=bd_proyecto_gnu
        MYSQL_PORT=3306
        JWT_SECRET=jwt_secret
        JWT_EXPIRESIN=8h
    </pre>


### Inicializar la aplicación

> **nest start "mode production"**

> **nest start --watch "mode development"**

## Servidor de desarrollo

Navegar a `http://localhost:3000/api/`. El servicio se ejecutara si su medio es el adecuado