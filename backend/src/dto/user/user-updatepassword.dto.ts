import { Length, IsNotEmpty } from 'class-validator';
export class UserUpdatepasswordDto {

    @IsNotEmpty()
    iduser: number;

    @IsNotEmpty()
    @Length(6, 100)
    password: string;

}
