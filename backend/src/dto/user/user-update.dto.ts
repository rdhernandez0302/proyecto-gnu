import { IsEmail, Length, IsString, IsNotEmpty, IsEmpty } from 'class-validator';
import { RoleEntity } from 'src/entity/role.entity';

export class UserUpdateDto {

    @IsNotEmpty()
    iduser: number;

    @IsNotEmpty()
    @Length(0, 100)
    @IsString()
    name: string;

    @IsNotEmpty()
    @Length(0, 100)
    @IsString()
    lastName: string;

    @IsNotEmpty()
    @IsEmail()
    @Length(0, 100)
    email: string;

    @IsNotEmpty()
    @Length(3, 100)
    @IsString()
    username: string;

    @IsNotEmpty()
    idrole: RoleEntity;

}
