import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/keys.config';
import { DatabaseConfig } from './config/database.config';
import { UsersModule } from './module/users.module';
import { HttpExceptionFilterMessage } from './config/http-exception-message.config';
import { APP_FILTER } from '@nestjs/core';
import { HttpErrorFilter } from './config/http-error-filter.config';
import { RolesModule } from './module/roles.module';

@Module({
  imports: [
    
    UsersModule,
    RolesModule,
    ...DatabaseConfig,
    ConfigModule,
  ],
  controllers: [],
  providers: [
    AppService,
    HttpExceptionFilterMessage,
    {
        provide: APP_FILTER,
        useClass: HttpErrorFilter,
    },
    {
        provide: ConfigService,
        useValue: new ConfigService(),
    },
  ],
  exports: [ConfigService, HttpExceptionFilterMessage],
})
export class AppModule {

  static port: number | string;
  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get(Configuration.PORT);
  }

}
