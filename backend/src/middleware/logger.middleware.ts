import { Injectable, NestMiddleware, BadRequestException, RequestMethod, Param, UnauthorizedException } from '@nestjs/common';
import { HttpExceptionFilterMessage } from '../config/http-exception-message.config';
import { PERMISSION_TREE_PUBLIC, API_NAME, PERMISSION_TREE_PROTECTED } from '../config/constants';
import { Request } from 'express-serve-static-core';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(
              private _httpExceptionFilterMessage: HttpExceptionFilterMessage,
              private _jwtService: JwtService) {}
  async use(req: Request, res: any, next: () => void) {

    let route = req.route.path;

    route = route.replace(`/${API_NAME}/`, '');

    for (const [key, value] of Object.entries(req.params)) {
      route = route.replace(`/:${key}`, '');
    }

    const constraints = { idRouteNotExist: 'No existe la ruta en el sistema' };
    const message = this._httpExceptionFilterMessage.Message(null, null, null, constraints);
    throw new BadRequestException(message);
  }

}
