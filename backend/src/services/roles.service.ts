
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoleEntity } from '../entity/role.entity';

@Injectable()
export class RolesService {
    constructor(@InjectRepository(RoleEntity) private _rolesRepository?: Repository<RoleEntity>) {}
    
    async getByData(_data) {
        return await this._rolesRepository.find({
            where: [_data],
        });
    }

    async getByFilter(_datatableRequest): Promise<RoleEntity[]>{
        let query = 'SELECT r.* FROM role r ';
        
        if(_datatableRequest.search.value){
            query += `WHERE (r.name LIKE "%${_datatableRequest.search.value}%" OR r.main_route LIKE "%${_datatableRequest.search.value}%") `;
        }

        if(_datatableRequest.order[0]){
            query += `ORDER BY ${_datatableRequest.order[0].active} ${_datatableRequest.order[0].direction} `;
        }else{
            query += 'ORDER BY idrole ASC ';
        }

        if(_datatableRequest.length != -1 )
        {
            query += `LIMIT ${_datatableRequest.start}, ${_datatableRequest.length}`;
        }
        return await this._rolesRepository.query(query);
    }

    async getCountByFilters(_busqueda: string): Promise<RoleEntity[]>{
        let query = 'SELECT count(*) as count FROM role r ';
        if( _busqueda) {
            query += `WHERE (r.name LIKE "%${_busqueda}%" OR r.main_route LIKE "%${_busqueda}%") `;
        }
        return await this._rolesRepository.query(query);
    }


}
