import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../entity/user.entity';
import { UserCreateDto } from '../dto/user/user-create.dto';

@Injectable()
export class UsersService {

    constructor(@InjectRepository(UserEntity) private _usersRepository: Repository<UserEntity>) {}

    async getByData(_data) {
        return await this._usersRepository.find({
            where: [_data],
            select: ['iduser', 'name', 'lastName', 'email', 'username', 'status', 'idrole'],
        });
    }

    async getOneByData(_data: any): Promise<UserEntity> {

        let query = `SELECT u.*, u.last_name as lastName, r.name AS name_role FROM user AS u
                                INNER JOIN role as r ON u.idrole = r.idrole `;
        let contador = 1;
        for (const [key, value] of Object.entries(_data)) {
            
            if(contador === 1){
                query += 'WHERE ';
            }else{
                query += 'AND ';
            }

            query += `${key} = '${value}' `;

            contador=2;
        }
        const user = await this._usersRepository.query(query);
        return user.length > 0 ? user[0] : false;
    }

    async update(_iduser: number, _user): Promise<UserEntity>  {
        await this._usersRepository.update(_iduser, _user);
        const data = await this._usersRepository.findOne(_iduser);
        delete data.password;
        return data;
    }

    async delete(_user: UserEntity) {
        this._usersRepository.delete(_user);
    }


    async createDynamic(_user: any)  {
        const data = this._usersRepository.create(_user);
        await this._usersRepository.save(data);
        return data;
    }

    async create(_user: UserCreateDto): Promise<UserEntity>  {
        const data = this._usersRepository.create(_user);
        await this._usersRepository.save(data);
        delete data.password;
        return data;
    }

    async getByFilter(_datatableRequest): Promise<UserEntity[]>{
        let query = `SELECT u.*, r.name AS name_role FROM user AS u
                        INNER JOIN role as r ON u.idrole = r.idrole `;
        
        if(_datatableRequest.search.value){
            query += `WHERE (u.name LIKE "%${_datatableRequest.search.value}%" OR u.username LIKE "%${_datatableRequest.search.value}%" OR u.email LIKE "%${_datatableRequest.search.value}%" OR r.name LIKE "%${_datatableRequest.search.value}%") `;
        }

        if(_datatableRequest.order[0]){
            query += `ORDER BY ${_datatableRequest.order[0].active} ${_datatableRequest.order[0].direction} `;
        }else{
            query += 'ORDER BY iduser ASC ';
        }

        if(_datatableRequest.length != -1 )
        {
            query += `LIMIT ${_datatableRequest.start}, ${_datatableRequest.length}`;
        }
        return await this._usersRepository.query(query);
    }

    async getCountByFilters(_busqueda: string): Promise<UserEntity[]>{
        let query = `SELECT count(*) AS count FROM user AS u
                        INNER JOIN role as r ON u.idrole = r.idrole `;
        if( _busqueda) {
            query += `WHERE (u.name LIKE "%${_busqueda}%" OR u.username LIKE "%${_busqueda}%" OR u.email LIKE "%${_busqueda}%" OR r.name LIKE "%${_busqueda}%") `;
        }
        return await this._usersRepository.query(query);
    }
}
