import { RoleEntity } from '../entity/role.entity';
export interface IJwtPayload {
    iduser: number;
    username: string;
    email: string;
    iat?: Date;
    idrole: RoleEntity;
}
