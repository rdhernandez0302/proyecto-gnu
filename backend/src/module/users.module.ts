import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from '../services/users.service';
import { UsersController } from 'src/controllers/users.controller';
import { RolesService } from '../services/roles.service';
import { HttpExceptionFilterMessage } from '../config/http-exception-message.config';
import { UserEntity } from '../entity/user.entity';
import { RoleEntity } from '../entity/role.entity';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { Configuration } from '../config/keys.config';

@Module({
    imports: [TypeOrmModule.forFeature([UserEntity, RoleEntity]), 
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory(config: ConfigService) {
                return {
                    secret: config.get(Configuration.JWT_SECRET),
                };
            }
        })
    ],
    providers: [UsersService, RolesService, HttpExceptionFilterMessage],
    controllers: [UsersController],
    exports: [RolesService, UsersService, HttpExceptionFilterMessage],
})
export class UsersModule {}
