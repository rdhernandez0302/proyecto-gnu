import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesController } from '../controllers/roles.controller';
import { RolesService } from '../services/roles.service';
import { RoleEntity } from '../entity/role.entity';
import { HttpExceptionFilterMessage } from 'src/config/http-exception-message.config';

@Module({
    imports: [TypeOrmModule.forFeature([RoleEntity])],
    providers: [RolesService, HttpExceptionFilterMessage],
    controllers: [RolesController],
    exports: [HttpExceptionFilterMessage]
})
export class RolesModule {}
