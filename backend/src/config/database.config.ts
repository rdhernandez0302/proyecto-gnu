import { TypeOrmModule } from '@nestjs/typeorm';
import { Configuration } from './keys.config';
import { ConfigService } from './config.service';
import { ConfigModule } from './config.module';
import { ConnectionOptions } from 'typeorm';

export const DatabaseConfig = [
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    async useFactory(config: ConfigService) {
      return {
          type: 'mysql',
          host: config.get(Configuration.MYSQL_HOST),
          port: Number(config.get(Configuration.MYSQL_PORT)),
          username: config.get(Configuration.MYSQL_USERNAME),
          password: config.get(Configuration.MYSQL_PASSWORD),
          database: config.get(Configuration.MYSQL_DATABASE),
          entities: [__dirname + '/../**/*.entity{.ts,.js}'],
          migrations: [__dirname + '/migrations/*{.ts,.js}'],
          synchronize: true,
      } as ConnectionOptions;
    },
  }),
];
