export enum Configuration {
    PORT            = 'PORT',
    MYSQL_HOST      = 'MYSQL_HOST',
    MYSQL_USERNAME  = 'MYSQL_USERNAME',
    MYSQL_PASSWORD  = 'MYSQL_PASSWORD',
    MYSQL_DATABASE  = 'MYSQL_DATABASE',
    MYSQL_PORT      = 'MYSQL_PORT',
    JWT_SECRET      = 'JWT_SECRET',
    JWT_EXPIRESIN   = 'JWT_EXPIRESIN',
}
