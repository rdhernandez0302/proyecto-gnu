
/*
|--------------------------------------------------------------------------
| status permission tree
|--------------------------------------------------------------------------
|
| permission tree
|
*/
export const PERMISSION_TREE_PUBLIC = 1;
export const PERMISSION_TREE_PRIVATE = 2;
export const PERMISSION_TREE_PROTECTED = 3;

/*
|--------------------------------------------------------------------------
| status permission tree
|--------------------------------------------------------------------------
|
| permission tree
|
*/
export const USER_ACTIVE = 1;
export const USER_INACTIVE = 0;

/*
|--------------------------------------------------------------------------
| name api
|--------------------------------------------------------------------------
|
| name api
|
*/
export const API_NAME = 'api';
