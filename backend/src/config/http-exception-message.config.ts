export class HttpExceptionFilterMessage {
    private target: any;
    private value: any;
    private property: any;
    private constraints: any;

    Message(_target?: any, _value ?: any, _property ?: any, _constraints ?: any) {

        
        this.target = (_target) ? _target : '';
        this.value = (_value) ? _value : '';
        this.property = (_property) ? _property : '';
        this.constraints = (_constraints) ? _constraints : '';
    
        const message = [
            {
                target: this.target,
                value: this.value,
                property: this.property,
                children: [],
                constraints: this.constraints,
            },
        ];

        return message;
    }
}
