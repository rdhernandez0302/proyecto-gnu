import { Catch, ExceptionFilter, HttpException, ArgumentsHost, HttpStatus } from '@nestjs/common';
import { getStatusText } from 'http-status-codes';

@Catch()
export class HttpErrorFilter implements ExceptionFilter {

    catch(exception: HttpException, host: ArgumentsHost) {

        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();

        const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        let message = exception.message;

        if ( ! exception.message.hasOwnProperty('statusCode') || ! exception.message.hasOwnProperty('error') ) {
            message = {
                statusCode: status,
                error: getStatusText(status),
                message: exception.message,
            };
        }

        response.status(status).json(message);
    }
}
