import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, BeforeInsert, UpdateDateColumn } from 'typeorm';
import { RoleEntity } from './role.entity';
import * as bcrypt from 'bcryptjs';

@Entity('user')
export class UserEntity {

    @PrimaryGeneratedColumn()
    iduser: number;

    @Column({length: 255})
    name: string;

    @Column({length: 255, name: 'last_name' })
    lastName: string;
    
    @Column({length: 255, default: '', name: 'profile_photo' })
    profilePhoto: string;

    @Column({length: 255, unique: true})
    email: string;

    @Column({length: 255, unique: true})
    username: string;

    @Column({length: 255})
    password: string;

    @Column('tinyint', {default: 1 })
    status: number;

    @ManyToOne(type => RoleEntity)
    @JoinColumn({ name: 'idrole' })
    @Column({nullable: false})
    idrole: RoleEntity;

    @Column({ name: 'created_at' })
    createdAt: Date;

    @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
    updatedAt: Date;

    @BeforeInsert()
    createdDate() {
        this.createdAt = new Date();
    }

    async comparePassword(attempt: string) {
        return await bcrypt.compare(attempt, String(this.password));
    }
}
