import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, UpdateDateColumn } from 'typeorm';

@Entity('role')
export class RoleEntity {

    @PrimaryGeneratedColumn()
    idrole: number;

    @Column({length: 100})
    name: string;

    @Column({ name: 'created_at' })
    createdAt: Date;

    @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
    updatedAt: Date;

    @BeforeInsert()
    createdDate() {
        this.createdAt = new Date();
    }

}
