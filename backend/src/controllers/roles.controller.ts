import { Controller, Get, Param, Query, Body, Post, HttpException, HttpStatus, BadRequestException } from '@nestjs/common';
import { RolesService } from '../services/roles.service';
import { HttpExceptionFilterMessage } from 'src/config/http-exception-message.config';

import { PERMISSION_TREE_PRIVATE } from '../config/constants';


@Controller('roles')
export class RolesController {

    constructor(private _rolesService: RolesService,
                private _httpExceptionFilterMessage: HttpExceptionFilterMessage) {}

    @Get('list')
    list() {
        return this._rolesService.getByData({});
    }
}
