import { Controller, Post, Body, Get, Param, BadRequestException, HttpException, HttpStatus, Req, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../services/users.service';
import { RolesService } from '../services/roles.service';
import { HttpExceptionFilterMessage } from '../config/http-exception-message.config';
import { UserCreateDto } from 'src/dto/user/user-create.dto';
import { UserUpdateDto } from 'src/dto/user/user-update.dto';
import { UserUpdatepasswordDto } from 'src/dto/user/user-updatepassword.dto';
import * as bcrypt from 'bcryptjs';

@Controller('users')
export class UsersController {

    constructor(private _usersService: UsersService,
                private _rolesService: RolesService,
                private _httpExceptionFilterMessage: HttpExceptionFilterMessage) {}

    @Get('load-data')
    async loadData() {
        const salt = bcrypt.genSaltSync(10);
        const dataIdrole = { idrole: 1 };
        const role = await this._rolesService.getByData(dataIdrole);
        const password =  await bcrypt.hash(String("123456"), salt);
      
        for (let i = 2; i <= 3000; i++) {
            let dataUserCreateDto = {
                name: 'ronald' + i,
                lastName: 'hernandez' + i,
                username: 'rdhernandez' + i,
                email: 'rdhernandez' + i + '@gmail.com',
                idrole: '1',
                password,
                status:1,
                iduser: null,
            };
            console.log(dataUserCreateDto);
            let data = await this._usersService.createDynamic(dataUserCreateDto);
        }
        
        return this._usersService.getByData({});
    }

    @Get('list')
    list() {
        return this._usersService.getByData({});
    }

    @Get('list-by-id/:iduser')
    async listById(@Param() _params) {
        const data = await this._usersService.getOneByData({iduser:_params.iduser});
        delete data.password;
        return data;
    }

    @Post('change-status')
    async changeStatus(@Body() body) {

        const user = await this._usersService.getOneByData({iduser: body.iduser });

        if (!user) {
            const constraints = { idUserNotExist: 'El usuario no posee un identificador válido' };
            const message = this._httpExceptionFilterMessage.Message(body, body.iduser, 'iduser', constraints);
            throw new BadRequestException(message);
        }
        let status = 1;
        let name = 'activo';
        if(user.status == 1){
            status = 2;
            name = 'inactivo';
        }

        const data = await this._usersService.update(body.iduser, {status});

        if (! data) {
            const constraints = {
                isUserNotUpdate: 'El usuario no se modificó correctamente',
            };
            const message = this._httpExceptionFilterMessage.Message(body, null, null, constraints);
            throw new BadRequestException(message);
        }

        const constraints = {
            isUpdate: `¡El usuario se ${name} correctamente!`,
        };
        const message = this._httpExceptionFilterMessage.Message(null, data, null, constraints);
        throw new HttpException(message, HttpStatus.OK);
    }

    @Post('change-password')
    async changePassword(@Body() _userUpdatepasswordDto: UserUpdatepasswordDto) {

        const user = await this._usersService.getOneByData({iduser: _userUpdatepasswordDto.iduser });

        if (!user) {
            const constraints = { idUserNotExist: 'El usuario no posee un identificador válido' };
            const message = this._httpExceptionFilterMessage.Message(_userUpdatepasswordDto, _userUpdatepasswordDto.iduser, 'iduser', constraints);
            throw new BadRequestException(message);
        }

        const salt = bcrypt.genSaltSync(10);
        _userUpdatepasswordDto.password = await bcrypt.hash(String(_userUpdatepasswordDto.password), salt);
       
        const data = await this._usersService.update(_userUpdatepasswordDto.iduser, _userUpdatepasswordDto);

        if (! data) {
            const constraints = {
                isUserNotUpdate: 'El usuario no se modificó correctamente',
            };
            const message = this._httpExceptionFilterMessage.Message(_userUpdatepasswordDto, null, null, constraints);
            throw new BadRequestException(message);
        }

        const constraints = {
            isUpdate: `¡El usuario se modificó correctamente!`,
        };
        const message = this._httpExceptionFilterMessage.Message(null, data, null, constraints);
        throw new HttpException(message, HttpStatus.OK);
    }

    @Post('create')
    async create(@Body() _userCreateDto: UserCreateDto) {

        const dataIdrole = {idrole:Number(_userCreateDto.idrole)};
        const role = await this._rolesService.getByData(dataIdrole);

        if ( role.length === 0) {
            const constraints = { idRoleNotExist: 'El rol no posee un identificador válido' };
            const message = this._httpExceptionFilterMessage.Message(_userCreateDto, _userCreateDto.idrole, 'idrole', constraints);
            throw new BadRequestException(message);
        }

        const findByUsername = await this._usersService.getOneByData({username: _userCreateDto.username});

        if ( findByUsername) {
            const constraints = { isUsernameExist: 'El nombre de usuario ya existe, por favor ingrese otro nuevamente' };
            const message = this._httpExceptionFilterMessage.Message(_userCreateDto, _userCreateDto.idrole, 'idrole', constraints);
            throw new BadRequestException(message);
        }

        const findByEmail = await this._usersService.getOneByData({email: _userCreateDto.email});

        if ( findByEmail ) {
            const constraints = { isEmailExist: 'El correo electrónico ya existe, por favor ingrese otro nuevamente' };
            const message = this._httpExceptionFilterMessage.Message(_userCreateDto, _userCreateDto.idrole, 'idrole', constraints);
            throw new BadRequestException(message);
        }

        const salt = bcrypt.genSaltSync(10);
        _userCreateDto.password = await bcrypt.hash(String(_userCreateDto.password), salt);

        const data = await this._usersService.create(_userCreateDto);

        if (! data) {
            const constraints = {
                isUserNotSave: 'El usuario no se creo correctamente',
            };
            const message = this._httpExceptionFilterMessage.Message(_userCreateDto, null, null, constraints);
            throw new BadRequestException(message);
        }

        const constraints = {
            isSave: '¡El usuario se guardo correctamente!',
        };
        const message = this._httpExceptionFilterMessage.Message(null, data, null, constraints);
        throw new HttpException(message, HttpStatus.OK);
    }

    @Post('update')
    async update(@Body() _userUpdateDto: UserUpdateDto) {

        const dataIdrole = {idrole:Number(_userUpdateDto.idrole)};
        const role = await this._rolesService.getByData(dataIdrole);

        if ( role.length === 0) {
            const constraints = { idRoleNotExist: 'El rol no posee un identificador válido' };
            const message = this._httpExceptionFilterMessage.Message(_userUpdateDto, _userUpdateDto.idrole, 'idrole', constraints);
            throw new BadRequestException(message);
        }

        const findByUsername = await this._usersService.getOneByData({username: _userUpdateDto.username});

        if ( findByUsername) {
            if(_userUpdateDto.iduser != findByUsername.iduser){
                const constraints = { isUsernameExist: 'El nombre de usuario ya existe, por favor ingrese otro nuevamente' };
                const message = this._httpExceptionFilterMessage.Message(_userUpdateDto, _userUpdateDto.idrole, 'idrole', constraints);
                throw new BadRequestException(message);   
            }
        }

        const findByEmail = await this._usersService.getOneByData({email: _userUpdateDto.email});

        if ( findByEmail ) {
            if(_userUpdateDto.iduser != findByEmail.iduser){
                const constraints = { isEmailExist: 'El correo electrónico ya existe, por favor ingrese otro nuevamente' };
                const message = this._httpExceptionFilterMessage.Message(_userUpdateDto, _userUpdateDto.idrole, 'idrole', constraints);
                throw new BadRequestException(message);
            }
        }

        const data = await this._usersService.update(_userUpdateDto.iduser, _userUpdateDto);

        if (! data) {
            const constraints = {
                isUserNotUpdate: 'El usuario no se modificó correctamente',
            };
            const message = this._httpExceptionFilterMessage.Message(_userUpdateDto, null, null, constraints);
            throw new BadRequestException(message);
        }

        const constraints = {
            isUpdate: '¡El usuario se modificó correctamente!',
        };
        const message = this._httpExceptionFilterMessage.Message(null, data, null, constraints);
        throw new HttpException(message, HttpStatus.OK);
    }

    async dataRecover(_datatableRequest, _data){
        let datas =  [];
        let filtered_rows = _data.length;
        for (let i = 0; i < _data.length; i++) {
            let sub_array = [];
            sub_array[0] = _data[i]['iduser'];   
            sub_array[1] = _data[i]['name']; 
            sub_array[2] = _data[i]['username']; 
            sub_array[3] = _data[i]['email'];   
            sub_array[4] = _data[i]['name_role'];
            sub_array[5] = 'action';
            sub_array[6] = _data[i]['status'];
            datas[i] = sub_array; 
        }
    
        let busqueda='';
        if(_datatableRequest.search.value){
            busqueda += _datatableRequest.search.value;
        }

        const countFilter = await this._usersService.getCountByFilters(busqueda);

        const output = {
            draw: 1,
            recordsTotal: filtered_rows,
            recordsFiltered: Number(countFilter[0]['count']),
            data: datas,
        };

        return output;
    }

    @Post('list-users')
    async listUsers(@Body() _datatableRequest) {
        const data = await this._usersService.getByFilter(_datatableRequest);
        const output = await this.dataRecover(_datatableRequest, data);
        return output;
    }

}
