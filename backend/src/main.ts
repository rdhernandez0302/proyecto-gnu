import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { useContainer } from 'typeorm';
import { ValidationPipe } from '@nestjs/common';
import * as fs from 'fs';
import { API_NAME } from './config/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.useGlobalPipes(new ValidationPipe({whitelist: true}));
  app.setGlobalPrefix(API_NAME);
  app.enableCors();
  await app.listen(AppModule.port);
  const server = app.getHttpServer();
  const router = server._events.request._router.stack;

  const path = './src/assets/template/index.txt';
  if( fs.existsSync(path) ){
    fs.unlinkSync(path);
  }
  
  router.forEach(function(r){
    if (r.route && r.route.path){
      fs.appendFile(path, r.route.path + '\n', (error) =>{
        if(error){
         throw error;
        }
      });
    }
  });
}
bootstrap();
